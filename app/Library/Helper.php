<?php

namespace App\Library;

class Helper {
    public static function res($status = 1, $message = "Success", $data = null){
        return [
            'status' => $status,
            'message' => $message,
            'data' => $data
        ];
    }
    public static function uploadImage($image) {
        if(!in_array($image->getClientOriginalExtension(),['png','jpg'])){
            return self::res(0,"Img invalid");
        }
        if($image->getSize() > 500000000){
            return self::res(0, "Img lager");
        }
        $fileName = time().$image->getClientOriginalName();
        $image->move('img/upload',$fileName);
        $dataRes = [
            'url' => "/img/upload/".$fileName
        ];
        
        return self::res(1,"Success",$dataRes);
    }


    public static function slugIfy($str, $delimiter = '-')
    {
        $str = trim(mb_strtolower(preg_replace('/[\s-]+/', $delimiter, $str), 'UTF-8'));
        $str = preg_replace('~[^\pL\d]+~u', $delimiter, $str);
        $str = preg_replace('~-+~', $delimiter, $str);
        $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
        $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
        $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
        $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
        $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
        $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
        $str = preg_replace('/(đ)/', 'd', $str);
        $str = preg_replace('/[^a-z0-9-\s]/', '', $str);
        return $str;
    }
}