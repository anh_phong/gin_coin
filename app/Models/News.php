<?php

namespace App\Models;

use App\Library\Helper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    use HasFactory;

    const STATUS_ACTIVE = 1;
    const STATUS_UN_ACTIVE = 2;

    // const TYPE_HIGHTLIGHT = 1;
    const TYPE_NEW = 1;

    protected $guarded = [];


    protected $attributes = [
        'status' => self::STATUS_ACTIVE,
        'type' => self::TYPE_NEW,
    ];

    // public function setSlugAttribute()
    // {
    //     return $this->attributes['slug'] = Helper::slugIfy($this->attributes['title']);
    // }
  
}
