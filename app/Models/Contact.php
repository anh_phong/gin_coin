<?php

namespace App\Models;

use App\Models\BaseModel;

class Contact extends BaseModel
{

    protected $table = 'contacts';

    protected $attributes = [
        'status' => 2
    ];

    protected $fillable = ['name','email','subject','message','discussion'];
}
