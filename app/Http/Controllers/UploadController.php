<?php

namespace App\Http\Controllers;

use App\Library\Helper;
use Illuminate\Http\Request;
use Monolog\Handler\IFTTTHandler;

class UploadController extends Controller
{
    //
    public function response_ajax($status = 1, $message = "Success", $data = null, $option = null)
    {
        # code...
        $res = [
            'status' => $status,
            'message' => $message,
            'data' => $data,
            'option' => $option,
        ];
        response()->json($res)->header('Content-Type', 'application/json')->send();
    }
    public function ajax_image(Request $request)
    {
        $image = $request->image;
        $upload = Helper::uploadImage($image);
        return $this->response_ajax($upload['status'], $upload['message'], $upload['data']);
    }

    public function upload_image(Request $request)
    {
        $request->validate([
            'file' => "required"
        ]);
        $file = $request->file;
        $validate = $this->validateImg($file);
        if (!$validate['status']) {
            return false;
        }
        $fileName = time().$file->getClientOriginalName();
        $path = '/img/upload/' . $fileName;
        $file->move('img/upload/', $fileName);
        return json_encode(['location' => $path]);
    }

    public function validateImg($image)
    {
    
        $accept = ['png','jpg','gif','jpeg'];
        $max = 500000000;
        if(!in_array($image->getClientOriginalExtension(),$accept)){
            return [
                'status' => false,
                'message' => "Image invalid"
            ];
        }
        if($image->getSize() > $max){
            return [
                'status' => false,
                'message' => "Img lager"
            ];
        }
        
        return [
            'status' => true,
            'message' => "Success"
        ];
    }
}
