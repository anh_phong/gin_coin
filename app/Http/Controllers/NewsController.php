<?php

namespace App\Http\Controllers;

use App\Library\Helper;
use App\Models\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    //
    public function get_edit($id)
    {
        $new = News::find($id);
        return view('admin.news.edit', compact('id', 'new'));
    }
    public function get_create()
    {
        return view('admin.news.edit');
    }

    public function store(Request $request)
    {
        $validate = [
            'title' => "required",
            'content' => "required",
        ];
        if (!$request->input('new_id')) {
            $validate['logo'] = 'required|image';
        }
        $request->validate($validate);

        $data = $request->all();

        if (@$request->logo) {
            $logo = $request->logo;
            $upload  = Helper::uploadImage($logo);
            if (!$upload['status']) {
                return redirect()->back()->withInput()->with('error', $upload['message']);
            }
            $data['logo'] = $upload['data']['url'];
        }
        $saveNew = new News();
        if ($request->input('new_id')) {
            $saveNew = $saveNew::find($request->input('new_id'));
        }
        if (@$data['logo']) {
            $saveNew->logo = $data['logo'];
        }
        $saveNew->title = $data['title'];
        $saveNew->content = $data['content'];
        $saveNew->slug = Helper::slugIfy($data['title'])."-".time();
        $saveNew->save();
        return redirect()->route('dashboard');
    }
    public function delete($id)
    {
        $new = News::find($id);
        $new->delete();
        return redirect()->back();
    }
}
