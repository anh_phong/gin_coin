<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Contact;
use App\Models\News;
use Facade\FlareClient\View;

class HomeController extends Controller
{
    //

    public function index()
    {
        # code...
        $news =  News::where('status', News::STATUS_ACTIVE)->orderByDesc('id')->limit(5)->get();
        return view('home',compact('news'));
    }

    public function news($page = 1, $limit = 25)
    {
        # code...
        $offset = ($page-1)*$limit;
        $news =  News::where('status', News::STATUS_ACTIVE)
            ->orderByDesc('id')
            ->skip($offset)
            ->take($limit)
            ->get();

        $totalPage = ceil(News::where('status', News::STATUS_ACTIVE)->count()/$limit);
        $range_page = [];

        for($i = -2; $i <= 2; $i++){
            if (($page + $i >0) && ($page + $i <= $totalPage)) {
                $range_page[] = $page + $i;
            }
        }
        $panigate = [
            'page' => $page,
            'limit' => $limit,
            'total_page' => $totalPage,
            'range_page' => $range_page
        ];


        return view('news',compact('news','panigate'));
    }

    public function get_new($slug)
    {
        # code...
        $new =  News::where('slug', $slug)->first();
        return view('new_detail',compact('new'));
    }
    public function save_contact(Request $request)
    {
        $request->validate([
            'name' => 'bail|required|max:255',
            'email' => 'bail|required|max:255',
            'subject' => 'bail|required|max:255',
            'message' => 'bail|required|max:255',
            'discussion' => 'bail|required|max:255',
        ]);
        $data = $request->except(['_token']);
        $contact = Contact::create($data);
        return View('contact',['category'=>'contact']);
    }

    public function contact_index($page = 1, $limit = 25)
    {
        $offset = ($page-1)*$limit;
        $contacts =  Contact::orderByDesc('id')
            ->skip($offset)
            ->take($limit)
            ->get();

        $totalPage = ceil(Contact::count()/$limit);
        $range_page = [];

        for($i = -2; $i <= 2; $i++){
            if (($page + $i >0) && ($page + $i <= $totalPage)) {
                $range_page[] = $page + $i;
            }
        }
        $panigate = [
            'page' => $page,
            'limit' => $limit,
            'total_page' => $totalPage,
            'range_page' => $range_page
        ];


        return view('admin.contact.index',compact('contacts','panigate'));
    }

    public function new_change_status($id)
    {
        $new = News::find($id);
        if (!$new) {
            return redirect()->back()->with('error', 'News not found');
        }
        $new->status = $new->status == News::STATUS_ACTIVE ? News::STATUS_UN_ACTIVE : News::STATUS_ACTIVE;
        $new->save();
        return redirect()->back()->with('success', 'Update success');
    }
    public function contact_change_status($id)
    {
        $contact = Contact::find($id);
        if (!$contact) {
            return redirect()->back()->with('error', 'Contact not found');
        }
        $contact->status = $contact->status == News::STATUS_ACTIVE ? News::STATUS_UN_ACTIVE : News::STATUS_ACTIVE;
        $contact->save();
        return redirect()->back()->with('success', 'Update success');
    }
}
