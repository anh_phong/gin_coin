<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        if(config('app.env') === 'production') {
            \URL::forceScheme('https');
        }

        view()->share('link_ios', 'https://apps.apple.com/app/gin-plus/id1566394421');
        // view()->share('link_android', 'https://play.google.com/store/apps/details?id=com.ginprox');
        view()->share('link_android', '/android');
    }
}
