<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "HomeController@index")->name('home');

Route::get('/news/total/{page?}', "HomeController@news")->name('news');

Route::get('/news/detail/{slug}', "HomeController@get_new")->name('new.detail');

Route::get('/team', function () {
    return view('team');
})->name('team');

Route::get('/whitepaper', function () {
    // $file = asset('img/gin_whitepaper.pdf');
    // $url = "https://docs.google.com/gview?embedded=true&url=".$file;
    // return redirect($url);
    return view('whitepaper');
})->name('whitepaper');

Route::get('/mining', function () {
    // $file = asset('img/gin-mining-pro.docx');
    // $url = "https://docs.google.com/gview?embedded=true&url=".$file;
    // return redirect($url);
    return view('mining');
})->name('mining');

Route::get('/contact', function () {
    return view('contact');
})->name('contact');

Route::get('/ginplus', function () {
    $join = "https://id.gincoin.co/authorize/login";
    return view('ginplus', compact('join'));
})->name('ginplus');

Route::get('/faq', function () {
    return view('faq');
})->name('faq');

Route::get('/download', function () {
    $iPod    = stripos($_SERVER['HTTP_USER_AGENT'], "iPod");
    $iPhone  = stripos($_SERVER['HTTP_USER_AGENT'], "iPhone");
    $iPad    = stripos($_SERVER['HTTP_USER_AGENT'], "iPad");

    // $redirect = 'https://play.google.com/store/apps/details?id=com.ginprox';
    $redirect = "https://id.gincoin.co/authorize/login";
    // $redirect = "https://apps.apple.com/app/gin-plus/id1566394421";
    // if ($iPod || $iPhone || $iPad) {
    //     return redirect($redirect);
    // }
    // $file= "file/gin-wallet-beta.apk";

    // return response()->download($file);
    return redirect($redirect);
})->name('download');

Route::get('/android', function(){
    // $file= "file/gin-wallet-beta.apk";
    $r = "https://drive.google.com/file/d/1CTPXB9KmnZCY4RpSBZNl8XGktVjcJv2K/view?usp=sharing";
    return redirect($r);
    // return response()->download($file);
})->name('android');

Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard', "DashboardController@index")->name('dashboard');

    Route::get('/news/edit/{id}', "NewsController@get_edit")->name('new.edit');
    Route::get('/news/create', "NewsController@get_create")->name('new.create');
    Route::post('/news/save', "NewsController@store")->name('new.save');

    Route::get('/news/delete/{id}', "NewsController@delete")->name('new.delete');
    Route::get('/news/update_status/{id}', "HomeController@new_change_status")->name('new.status');

    Route::get('/admin/contact', "HomeController@contact_index")->name('admin.contact');
    Route::get('/admin/contact_status/{id}', "HomeController@contact_change_status")->name('contact.status');

    //ajax

    Route::post('/upload/image', 'UploadController@upload_image')->name('upload.image');
});

require __DIR__ . '/auth.php';
