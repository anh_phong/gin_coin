@extends('index')

@section('content')
<div class="bg2 mw-1112">
<div class="layout">
    <div class="text-gradien uppercase text-center wow bounceInLeft">Team</div>
    <div class="mw-1112 wow bounceInUp" data-wow-duration="2s">
        <ul class="flex flex-wrap items-center">
            <li class="w-1/2 md:w-1/4 p-3 team-items">
                <div class="rounded-2xl shadow-lg hover:text-white cursor-pointer member">
                    <div class="py-16 px-2">
                        <img src="{{ asset('img/team1.jfif') }}" alt="" width="150" height="150" class="mx-auto border-4 border-white rounded-full">
                        <div class="team-name text-center pt-6 truncate" title="Chan Liam">Chan Liam</div>
                        <div class="team-des text-center pt-2 truncate" title="Market director">Market director</div>
                    </div>
                </div>
            </li>
            <li class="w-1/2 md:w-1/4 p-3 team-items">
                <div class="rounded-2xl shadow-lg hover:text-white cursor-pointer member">
                    <div class="py-16 px-2">
                        <img src="{{ asset('img/team2.jfif') }}" alt="" width="150" height="150" class="mx-auto border-4 border-white rounded-full">
                        <div class="team-name text-center pt-6 truncate" title="Jain Mask">Jain Mask </div>
                        <div class="team-des text-center pt-2 truncate" title="Leader">Leader</div>
                    </div>
                </div>
            </li>
            <li class="w-1/2 md:w-1/4 p-3 team-items">
                <div class="rounded-2xl shadow-lg hover:text-white cursor-pointer member">
                    <div class="py-16 px-2">
                        <img src="{{ asset('img/team3.jfif') }}" alt="" width="150" height="150" class="mx-auto border-4 border-white rounded-full">
                        <div class="team-name text-center pt-6 truncate" title="Levi Frida">Levi Frida </div>
                        <div class="team-des text-center pt-2 truncate" title="Technical operator">Technical operator</div>
                    </div>
                </div>
            </li>
            <li class="w-1/2 md:w-1/4 p-3 team-items">
                <div class="rounded-2xl shadow-lg hover:text-white cursor-pointer member">
                    <div class="py-16 px-2">
                        <img src="{{ asset('img/team4.jfif') }}" alt="" width="150" height="150" class="mx-auto border-4 border-white rounded-full">
                        <div class="team-name text-center pt-6 truncate" title="Walton Odile">Walton Odile</div>
                        <div class="team-des text-center pt-2 truncate" title="Blockchain specialist">Blockchain specialist</div>
                    </div>
                </div>
            </li>
            <li class="w-1/2 md:w-1/4 p-3 team-items">
                <div class="rounded-2xl shadow-lg hover:text-white cursor-pointer member">
                    <div class="py-16 px-2">
                        <img src="{{ asset('img/team5.jfif') }}" alt="" width="150" height="150" class="mx-auto border-4 border-white rounded-full">
                        <div class="team-name text-center pt-6 truncate" title="Abeer Elizabeth">Abeer Elizabeth</div>
                        <div class="team-des text-center pt-2 truncate" title="Technical director">Technical director</div>
                    </div>
                </div>
            </li>
            <li class="w-1/2 md:w-1/4 p-3 team-items">
                <div class="rounded-2xl shadow-lg hover:text-white cursor-pointer member">
                    <div class="py-16 px-2">
                        <img src="{{ asset('img/team11.jfif') }}" alt="" width="150" height="150" class="mx-auto border-4 border-white rounded-full">
                        <div class="team-name text-center pt-6 truncate" title="George Frank">George Frank </div>
                        <div class="team-des text-center pt-2 truncate" title="Technical operator">Technical operator</div>
                    </div>
                </div>
            </li>
            <li class="w-1/2 md:w-1/4 p-3 team-items">
                <div class="rounded-2xl shadow-lg hover:text-white cursor-pointer member">
                    <div class="py-16 px-2">
                        <img src="{{ asset('img/team6.jfif') }}" alt="" width="150" height="150" class="mx-auto border-4 border-white rounded-full">
                        <div class="team-name text-center pt-6 truncate" title="Lam Ning">Lam Ning </div>
                        <div class="team-des text-center pt-2 truncate" title="Managing director">Managing director</div>
                    </div>
                </div>
            </li>
           
            <li class="w-1/2 md:w-1/4 p-3 team-items">
                <div class="rounded-2xl shadow-lg hover:text-white cursor-pointer member">
                    <div class="py-16 px-2">
                        <img src="{{ asset('img/team7.jfif') }}" alt="" width="150" height="150" class="mx-auto border-4 border-white rounded-full">
                        <div class="team-name text-center pt-6 truncate" title="Kalila Julia">Kalila Julia </div>
                        <div class="team-des text-center pt-2 truncate" title="Financial experts">Financial experts</div>
                    </div>
                </div>
            </li>
            <li class="w-1/2 md:w-1/4 p-3 team-items">
                <div class="rounded-2xl shadow-lg hover:text-white cursor-pointer member">
                    <div class="py-16 px-2">
                        <img src="{{ asset('img/team8.jfif') }}" alt="" width="150" height="150" class="mx-auto border-4 border-white rounded-full">
                        <div class="team-name text-center pt-6 truncate" title="Jones Henry">Jones Henry </div>
                        <div class="team-des text-center pt-2 truncate" title="Technical operator">Technical operator</div>
                    </div>
                </div>
            </li>
           
            <li class="w-1/2 md:w-1/4 p-3 team-items">
                <div class="rounded-2xl shadow-lg hover:text-white cursor-pointer member">
                    <div class="py-16 px-2">
                        <img src="{{ asset('img/team10.jfif') }}" alt="" width="150" height="150" class="mx-auto border-4 border-white rounded-full">
                        <div class="team-name text-center pt-6 truncate" title="Dakshi John">Dakshi John  </div>
                        <div class="team-des text-center pt-2 truncate" title="Blockchain specialist">Blockchain specialist</div>
                    </div>
                </div>
            </li>
            <li class="w-1/2 md:w-1/4 p-3 team-items">
                <div class="rounded-2xl shadow-lg hover:text-white cursor-pointer member">
                    <div class="py-16 px-2">
                        <img src="{{ asset('img/team9.jfif') }}" alt="" width="150" height="150" class="mx-auto border-4 border-white rounded-full">
                        <div class="team-name text-center pt-6 truncate" title="Virtuous Helen">Virtuous Helen</div>
                        <div class="team-des text-center pt-2 truncate" title="Chief Technology Officer">Chief Technology Officer</div>
                    </div>
                </div>
            </li>
            
        </ul>
    </div>
</div>

</div>
@endsection