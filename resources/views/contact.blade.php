@extends('index')

@section('content')
<div class="bg2 mw-1112">
    <div class="layout pb-10">
        <div class="text-gradien uppercase text-center wow bounceInLeft">contact</div>
        <div class="contact-content w-3/4 xl:w-1/2 mx-auto text-content wow bounceInUp" data-wow-duration="2s">
            Company name: <strong>SAFE INVESTMENT GIN LTD</strong> <br>
            Full name: Walton Odile <br>
            Date Of Birth: 28 December 1983 <br><br>

            Language: English <br>
            Positions: DIRECTOR, SHAREHOLDER <br><br>

            <strong>Residential Address:</strong> 38 Stamford Road, Antrobus, CW9 2AU, United Kingdom <br>
            <strong>Service Address:</strong> 33 Cavendish Square, Marylebone, London W1G 0PW, United Kingdom <br><br>


            <strong>First 3 letters of the following for security</strong> <br>
            Mothers Maiden Name: Safe <br>
            Fathers Forename: Fun <br>
            Town of Birth: Gin <br>
            Shareholding value/ amount : £1 <br><br>


            <strong>Nature of business (SIC)</strong> <br>
            64301 - Activities of investment trusts <br>
            64302 - Activities of unit trusts <br>
            66110 - Administration of financial markets

            <div class="contact-question text-nav">
                <form action="" method="post">
                    {{csrf_field()}}
                    <div class="title">Have a question? Write a message</div>
                    <div class="flex flex-wrap">
                        <div class="md:flex-1">
                            <input type="text" name="name" placeholder="Name" class="contact-input" required>
                        </div>
                        <div class="md:flex-1">
                            <input type="email" name="email" placeholder="Email Address" class="contact-input" required>
                        </div>
                    </div>
                    <div class="flex flex-wrap">
                        <div class="md:flex-1">
                            <input type="text" name="subject" placeholder="Subject" class="contact-input" required>
                        </div>
                        <div class="md:flex-1">
                            <div class="discussion">
                                <div class="select flex justify-between items-center contact-input">
                                    <select name="discussion" required>
                                        <option value="" disabled selected>Discussion for</option>
                                        <option value="Login issue">Login issue</option>
                                        <option value="Change information">Change information</option>
                                        <option value="Download issue">Download issue </option>
                                        <option value="Other">Other</option>
                                    </select>
                                    <span><img src="/img/arrow_down.png" alt=""></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="flex">
                        <textarea name="message" id="" cols="30" rows="5" class="flex-1 contact-input" placeholder="Write Message ..." required></textarea>
                    </div>
                    @if ($errors->any())
                    <div class="">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li class="bg-red-500 rounded text-white my-2 px-3">{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="flex justify-center">
                        <button type="submit" class="contact-send">Send</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
@endsection