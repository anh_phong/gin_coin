<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Edit News') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    {{ __('Edit News') }}
                </div>
                <div class="p-6">
                    <form action="{{ route('new.save') }}" method="POST"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="">
                            <label for="">Logo</label>
                            @isset($new)
                                <img src="{{ $new->logo }}" alt="" width="100">
                            @endisset
                            <input type="file" name="logo">
                        </div>
                        <div class="py-6">
                            <input type="text" name="title" class="w-full rounded" placeholder="Title ..."
                                value="{{ isset($new) ? $new->title : '' }}">
                        </div>
                        <div class="py-6">
                            <textarea name="content" id="tinymce" cols="30" rows="50" class="w-full rounded"
                                placeholder="Content ...">{{ isset($new) ? $new->content : '' }}</textarea>
                        </div>
                        <div class="py-6">
                            @isset($id)
                                <input type="hidden" name="new_id" value="{{ $id }}">
                            @endisset
                            <div class="flex justify-center items-center">
                                <button type="submit" class="p-2 px-4 bg-indigo-500 hover:bg-indigo-700 rounded text-white mx-1">Save</button>
                                <a href="{{route('dashboard')}}" class="p-2 px-4 bg-gray-500 hover:bg-gray-700 rounded text-white mx-1">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
