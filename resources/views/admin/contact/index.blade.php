<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Contact') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    {{ __('List contact') }}
                </div>
                <div class="p-6">
                    <table id="example" class="display" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Discussion</th>
                                <th>Subject</th>
                                <th>Message</th>
                                <th>Readed</th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            @foreach ($contacts as $contact)
                            <tr>
                                <td>{{$contact->id}}</td>
                                <td>{{$contact->name}}</td>
                                <td>{{$contact->email}}</td>
                                <td>{{$contact->discussion}}</td>
                                <td>{{$contact->subject}}</td>
                                <td>{{$contact->message}}</td>
                                <td>
                                    <a href="{{route('contact.status', ['id'=>$contact->id])}}" class="block w-10 h-2 bg-gray-200 rounded-full mx-auto">
                                        <div class="w-4 h-4 rounded-full shadow transform -translate-y-1 {{ $contact->status == 1 ? 'translate-x-6 bg-green-400' : 'bg-gray-400' }}"></div>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    
                    @if ($panigate['total_page'] > 1)
                        <ul class="flex justify-center items-center pt-6">
                            @if ($panigate['page'] > 1)
                                <li>
                                    <a class="block w-8 h-8 flex justify-center items-center mx-1 rounded-full hover:bg-gray-300 " href="{{route('news',['page'=>$panigate['page']-1])}}"><</a>
                                </li>
                            @endif
                            @foreach ($panigate['range_page'] as $k => $page)
                                <li>
                                    <a class="block w-8 h-8 flex justify-center items-center mx-1 rounded-full hover:bg-gray-300 {{ $panigate['page'] == $page ? 'bg-gray-300' : '' }}" href="{{route('news',['page'=>$page])}}">{{$page}}</a>
                                </li>
                            @endforeach
                            @if ($panigate['page'] < $panigate['total_page'])
                                <li>
                                    <a class="block w-8 h-8 flex justify-center items-center mx-1 rounded-full hover:bg-gray-300" href="{{route('news',['page'=>$panigate['page']+1])}}">></a>
                                </li>
                            @endif
                        </ul>
                    @endif
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
