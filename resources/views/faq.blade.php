@extends('index')

@section('content')
<div class="bg2 mw-1112">
    <div class="layout pb-10">
        <div class="text-gradien uppercase text-center wow bounceInLeft">FREQUENTLY ASKED QUESTIONS</div>
        <div class="contact-content w-3/4 xl:w-1/2 mx-auto wow bounceInUp" data-wow-duration="2s">
            <ol>
                <li>
                    <strong>1. What is GIN?</strong>
                    <p>
                        <br>
                        GIN Blockchain is a new blockchain innovation for users to earn GIN,
                        a cryptocurrency that you can mine on your mobile phone (on Mainnet phase).
                        By playing the roles of Miners, Referrers and Verifiers,
                        players on the GIN Blockchain will be rewarded with GIN tokens.
                        To make GIN valuable, GIN Blockchain is specially designed as a network of righteous people,
                        encouraging players to exchange goods and services in real life with their GIN balance. and finally,
                        listing GIN on major cryptocurrency exchanges to trade GIN with fiat currency. <br>
                        Note that we say you can mine GIN in the Mainnet phase, but now you can do the task to receive GIN Token.
                    </p>
                    <br>
                </li>
                <li>
                    <strong>2. Can I withdraw my GIN now? When is the withdrawal time?</strong>
                    <p>
                        <br>
                        No, you still cannot withdraw GIN in phase 1.
                        Phase 2 of the GIN Blockchain roadmap is to list GIN Blockchain on major cryptocurrency exchanges,
                        allowing players to use GIN to earn. from GIN Blockchain to transactions and sales, as Q3 2022 target,
                        depending on the growth of the number of users and the dynamics of the community.
                        Please refer to our project roadmap in the White Paper: https://gincoin.co/white-paper
                        Do it takes time and collaborative effort to build value of the GIN community and the GIN Blockchain ecosystem
                        If you are looking to win the overnight lottery, please look elsewhere.
                    </p>
                    <br>
                </li>
                <li>
                    <strong>3. What is GIN's valuable growth potential?</strong>
                    <p>
                        <br>
                        The value of the GIN coin will be ensured by the GIN Blockchain community,
                        the volume of transactions in the exchange of goods or services between GIN Blockchain members,
                        liquidity in the transaction market and the moment of transaction.
                        Just like Bitcoin, the value was $ 0 at the time of its launch in 2008,
                        but as popularity increased and time passed,
                        1 Bitcoin is currently trading at $ 60,000 at time of writing.
                        Since it takes time and collaborative effort to build the value of the GIN coin and the GIN Blockchain ecosystem,
                        if you are looking to win the lottery overnight, please look elsewhere.
                    </p>
                    <br>

                </li>
                <li>
                    <strong>4. How much does it cost to earn GIN? Do I need to pay any money?</strong>
                    <p>
                        <br>
                        You do not have to pay any money to join GIN Blockchain and earn GIN.
                        However, this does not mean that the GIN is free.
                        To make the gaming experience on GIN Blockchain fun and rewarding,
                        we strive to build a network of true people, foster interaction among real-life players,
                        and encourage players. Exchange services or goods in real life with GIN balance.
                        Therefore, you need a consistent commitment to contribute to the community and the GIN is your reward.
                        The least you can do is login every day and activate a new mining session.
                    </p>
                    <br>

                </li>
                <li>
                    <strong>5. Why has growth been reduced?</strong>
                    <p>
                        <br>
                        To ensure the price stability of the GIN and prevent any common negative fluctuations between cryptocurrencies,
                        the supply cuts (otherwise known as 'halving' in the money world (electronic) is required to maintain the supply and demand chains of the GIN.
                        By reducing the supply of GIN tokens, GIN Blockchain can maintain the scarcity
                        of GIN tokens to ensure the value held by our GIN Blockchain users - including you.
                        Take Bitcoin as an example. Halving is a common event, happening about every four years for the largest and most valuable
                        cryptocurrencies in the world, and the price of Bitcoin actually goes up with every halving. In simpler words,
                        cutting the GIN token supply per key user milestone will protect the price of GINs while preventing price inflation.
                        <br>
                        According to our project roadmap, there will be a total of 4 halving increases in the entire project roadmap. The first halving is when we reach 1 million users with speed (-> 0.8 GIN / Hour), the 2nd drop is 5 million users reaching, the third is 10 million. , wave 4 drops when we reach 15 million users. Or when the entire amount of allotted coins is 100 million, the speed for Claim free to = 0 coin / h moves to phase 2 (pool 2)
                        <br>
                        While it may not be favorable for you to notice the gains in your coins are not as fast as it once was, this is an extremely important way to create and grow the value of GIN by reducing a half increase. For details, you can refer to the section “Why supply cutting is important on the GIN Blockchain” in our newsletter.
                    </p>
                    <br>

                </li>
                <li>
                    <strong>6. How do I earn GIN?</strong>
                    <p>
                        <br>
                        You earn GIN coins in 3 ways. First, you make money as Claim, by logging in daily to activate your next 24 hour Claim session. Once you have clicked the Claim button on the main screen, your GIN balance will increase by 1GIN / hour (or 0.8 GIN / hour or lower if you participate in the time when the GIN Blockchain has been decelerated. degrees) continuously for the next 24 hours in your basic hourly increments. Second, you earn extra GIN at 25% x your base gain x the number of active Claims in your team as Referrers by referring new members to the Claim team. account and maintain their operations. Third, you enjoy an additional basic gain as a Gamer, by participating in games made on the GIN Blockchain. Gamers are only done from the second stage.
                    </p>
                    <br>

                </li>
                <li>
                    <strong>7. Will GIN Blockchain drain my battery or data? Do I need to keep the app open?</strong>
                    <p>
                        <br>
                        Not for both of these sources, as GIN Blockchain is different from Bitcoin. You will only consume a negligible amount of battery and data when opening the GIN Blockchain application and activating your data mining session, the whole session can be done within 10 seconds. You don't need to open the app once clicked to activate a new mining session. GIN Blockchain application will not run in the background and consume any data and battery of your phone after you close the application. This is due to the difference between the consensus algorithm between GIN Blockchain and Bitcoin. For more technical details see our whitepaper.
                    </p>
                    <br>

                </li>
            </ol>
        </div>
    </div>

</div>
@endsection