<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>GIN COIN</title>
    <link rel="stylesheet" href="{{ asset('css/tailwind.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/modal-video.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/gin/main.css?v=1.11') }}">

    <link rel="icon" href="{{ asset('img/logo.png') }}">

    <!-- Start Alexa Certify Javascript -->
    <script type="text/javascript">
        _atrk_opts = {
            atrk_acct: "qQUVv1FYxz20cv",
            domain: "gincoin.co",
            dynamic: true
        };
        (function () {
            var as = document.createElement('script');
            as.type = 'text/javascript';
            as.async = true;
            as.src = "https://certify-js.alexametrics.com/atrk.js";
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(as, s);
        })();

    </script>
    <noscript><img src="https://certify.alexametrics.com/atrk.gif?account=qQUVv1FYxz20cv" style="display:none"
            height="1" width="1" alt="" /></noscript>
    <!-- End Alexa Certify Javascript -->

    <style>
        @media screen and (max-width: 768px) {
            #particles-js{
                height: 70%;
            }
        }
    </style>


</head>

<body>
    <div class="wrapper">
        <!-- particles.js container -->
        @yield('particles')

        <div class="shadow-lg sticky top-0 z-20 bg-white">
            <div class="px-3 xl:px-12 py-2">
                <div class="flex items-center">
                    <div class="w-1/2 md:w-1/4 md:pl-0">
                        <a href="/" class="flex items-center">
                            <div class="w-1/4">
                                <img src="{{ asset('img/logo.png') }}" alt="gin coin" class="max-w-full">
                            </div>
                            <div class="flex-1 pl-2 md:pl-6">
                                <img src="{{ asset('img/gin.png') }}" alt="gin coin" class="max-w-full">
                            </div>
                        </a>
                    </div>
                    <div class="flex-1 md:hidden flex justify-end" id="open-nav">
                        <img src="{{ asset('img/nav.png') }}" alt="nav" class="max-w-full">
                    </div>
                    <nav class="hidden md:block flex-1">
                        <ul class="flex items-center justify-end md:pr-6 text-nav">
                            <li class="mr-70 uppercase nav-items {{ Request::is('/') ? 'on' : '' }}">
                                <a href="{{ route('home') }}">HOME</a>
                            </li>
                            <li class="mr-70 uppercase nav-items {{ Request::is('news*') ? 'on' : '' }}">
                                <a href="{{ route('news') }}">NEWS</a>
                            </li>
                            <li class="mr-70 uppercase nav-items {{ Request::is('team*') ? 'on' : '' }}">
                                <a href="{{ route('team') }}">TEAM</a>
                            </li>
                            <li class="mr-76 uppercase nav-items {{ Request::is('whitepaper*') ? 'on' : '' }}">
                                <a href="{{ route('whitepaper') }}" target="_blank">whitepaper</a>
                            </li>
                            <li class="mr-72 uppercase nav-items {{ Request::is('mining*') ? 'on' : '' }}">
                                <a href="{{ route('mining') }}" target="_blank">MINING PRO</a>
                            </li>
                            <li class="mr-55 uppercase nav-items {{ Request::is('ginplus*') ? 'on' : '' }}">
                                <a href="{{ route('ginplus') }}" target="_blank">GIN PLUS</a>
                            </li>
                            <li
                                class="mr-55 uppercase nav-items menu_more_hover {{ Request::is('contact*') || Request::is('faq*') ? 'on' : '' }}">
                                <a href="javascript:;">MORE</a>
                                <div class="dropdown relative hidden">
                                    <ul class="absolute w-32 lg:w-40 rounded bg-white shadow p-3 md:p-6 menu-more"
                                        style="left: -5px">
                                        <li class="uppercase text-black {{ Request::is('contact*') ? 'on' : '' }}">
                                            <a href="{{ route('contact') }}">CONTACT</a>
                                        </li>
                                        <li class="uppercase text-black {{ Request::is('faq*') ? 'on' : '' }}">
                                            <a href="{{ route('faq') }}">FAQ</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li
                                class="px-2 xl:px-5 uppercase border-2 border-black radius-70 text-download hover-scale">
                                <a href="{{route('download')}}" class="block" target="_blank">JOIN NOW</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <nav class="bg-black text-white hidden md:hidden" id="nav-mobile">
                <ul class="text-center">
                    <li class="p-6 pb-3 font-semibold uppercase">
                        <a href="{{ route('home') }}">HOME</a>
                    </li>
                    <li class="p-3 font-semibold uppercase">
                        <a href="{{ route('news') }}">NEWS</a>
                    </li>
                    <li class="p-3 font-semibold uppercase">
                        <a href="{{ route('team') }}">TEAM</a>
                    </li>
                    <li class="p-3 font-semibold uppercase">
                        <a href="{{ route('whitepaper') }}" target="_blank">whitepaper</a>
                    </li>
                    <li class="p-3 font-semibold uppercase">
                        <a href="{{ route('contact') }}">CONTACT</a>
                    </li>
                    <li class="p-3 font-semibold uppercase">
                        <a href="{{ route('faq') }}">FAQ</a>
                    </li>
                    <li class="p-3 font-semibold uppercase">
                        <a href="{{ route('ginplus') }}" target="_blank">GIN PLUS</a>
                    </li>
                    <li class="p-6 pt-3 font-semibold uppercase">
                        <a href="{{ route('mining') }}" target="_blank">MINING PRO</a>
                    </li>
                </ul>
            </nav>
        </div>


        @yield('content')


        <div class="md:hidden phone-mobi {{ Route::currentRouteName() == 'home' ? 'home' : '' }}">
            <img src="{{ asset('img/phone1.png') }}" alt="" class="w-1/2 mx-auto">
        </div>

        <div class="bottom text-white {{ Route::currentRouteName() == 'home' ? 'home' : '' }}">
            <div class="flex justify-center download">
                <a href="{{route('download')}}" class="radius-70 border-2 border-white p-6 py-2 text-700S22 hover-scale"
                    id="download" target="blank">JOIN NOW</a>
            </div>
            <div class="pb-24">
                <div class="mw-883 px-12">
                    <div class="bg-white-blur border border-white rounded-2xl">
                        <div class="flex-row md:flex justify-center items-center p-6 md:px-0 py-6">
                            <a href="{{$link_android}}" class="block pb-3 md:p-3" target="_blank">
                                <img src="{{ asset('img/ggplay.png') }}" height="80" alt="" class="mx-auto">
                            </a>
                            <a href="{{$link_ios}}" class="block md:p-3" target="_blank">
                                <img src="{{ asset('img/app_store1.png') }}" height="80" alt="" class="mx-auto">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer>
            <div class="bg-black p-3">
                <div class="flex justify-center items-center">
                    <a href="https://twitter.com/GinBlockchain" class="p-5" target="_blank">
                        <img src="{{ asset('img/twitter.png') }}" height="80" alt="" class="bg-white">
                    </a>
                    <a href="https://www.linkedin.com/company/73014175/" class="p-5" target="_blank">
                        <img src="{{ asset('img/linkedin.png') }}" height="80" alt="" class="bg-white">
                    </a>
                    <a href="https://www.youtube.com/channel/UCDTXJoxb6msc4SHeg3mTtqQ" class="p-5" target="_blank">
                        <img src="{{ asset('img/youtube.png') }}" height="80" alt="" class="bg-white">
                    </a>
                    <a href="https://www.facebook.com/ginnetworks" class="p-5" target="_blank">
                        <img src="{{ asset('img/facebook.png') }}" height="80" alt="" class="bg-white">
                    </a>
                    <a href="http://t.me/gincoinchannel" class="p-5" target="_blank">
                        <img src="{{ asset('img/telegram.png?v=1.1') }}" height="80" alt="" class="bg-white">
                    </a>
                </div>
                <div class="text-white text-center text-content font-bold py-3">
                    Residential Address: 38 Stamford Road, Antrobus, CW9 2AU, United Kingdom <br>
                    Service Address: 33 Cavendish Square, Marylebone, London W1G 0PW, United Kingdom <br>
                    <a href="mailto:support@gincoin.co" class="pt-8 block">Support@gincoin.co</a>
                </div>
            </div>
        </footer>
    </div>
    <script src="{{ asset('js/jquery-3.5.1.min.js') }}"></script>
    <script src="{{ asset('js/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('js/wow.min.js') }}"></script>
    <script src="{{ asset('js/modal-video.min.js') }}"></script>
    @stack('js')

    <script>
        $(document).ready(function () {
            new WOW({
                mobile: false
            }).init();

            $("#open-nav").on("click", function () {
                $("#nav-mobile").toggleClass("hidden");
            })
            $(function () {
                $('a[href*=\\#]').on('click', function (e) {
                    e.preventDefault();
                    $('html, body').animate({
                        scrollTop: $($(this).attr('href')).offset().top
                    }, 500, 'easeInOutExpo');
                });
            });
            $(".js-video-button").modalVideo({
                youtube: {
                    controls: 0,
                    nocookie: true
                }
            });
        })

    </script>

</body>

</html>