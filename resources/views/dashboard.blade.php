<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('News') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200 flex justify-between items-center">
                    <div class="">{{ __('List News') }}</div>
                    <a href="{{ route('new.create') }}" class="p-2 px-4 bg-indigo-500 hover:bg-indigo-700 rounded text-white">Create</a>
                </div>
                <div class="p-6">
                    <table id="example" class="display" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Img</th>
                                <th>Title</th>
                                <th>Status</th>
                                <th>Option</th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            @foreach ($news as $new)
                            <tr>
                                <td>{{$new->id}}</td>
                                <td>
                                    <img src="{{$new->logo}}" alt="" width="100" class="mx-auto">
                                </td>
                                <td>
                                    <strong class="capitalize">{{$new->title}}</strong>
                                </td>
                                <td>
                                    <a href="{{route('new.status', ['id'=>$new->id])}}" class="block w-10 h-2 bg-gray-200 rounded-full mx-auto">
                                        <div class="w-4 h-4 rounded-full shadow transform -translate-y-1 {{ $new->status == 1 ? 'translate-x-6 bg-green-400' : 'bg-gray-400' }}"></div>
                                    </a>
                                </td>
                                <td>
                                    <div class="flex items-center justify-center">
                                        <a href="{{route('new.edit', ['id'=> $new->id] )}}" class="px-3">
                                            <img src="{{asset('img/edit.png')}}" alt="">
                                        </a>
                                        <a href="{{route('new.delete',['id'=> $new->id])}}" class="px-3">
                                            <img src="{{asset('img/trash.png')}}" alt="">
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
