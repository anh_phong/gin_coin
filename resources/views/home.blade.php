@extends('index')

@section('particles')
<div id="particles-js"></div> <!-- stats - count particles -->

@endsection

@section('content')

<div class="relative hidden md:block">
    <div class="flex justify-end">
        <img src="{{ asset('img/bg_top.png') }}" alt="" class="mw-944">
    </div>
    <div class="absolute top-0 pt-83 xl:pt-16 w-full z-10">
        <div class="flex justify-between items-start mw-1112">
            <div class="relative w-546">
                <img src="{{ asset('img/logo_desk.png') }}" alt="" class="mx-auto">
                <div class="absolute z-10">
                    <div class="text-title xl:py-12 uppercase">SAFE INVESTMENT GIN LTD</div>
                    <p class="text-content">
                        This is the year 2021, thinking about Crypto has changed. Owning cryptocurrency is just by
                        working hard every day.
                    </p>
                    <div class="hidden md:flex justify-center items-center p-6 md:px-0 py-6 relative">
                        <a href="{{$link_android}}" class="block pb-3 md:p-3 wow bounceInLeft" data-wow-delay="0.5s">
                            <img src="{{ asset('img/ggplay.png') }}" height="80" alt="" class="mx-auto">
                        </a>
                        <a href="{{$link_ios}}" class="block md:p-3 wow bounceInLeft">
                            <img src="{{ asset('img/app_store1.png') }}" height="80" alt="" class="mx-auto">
                        </a>
                        <div class="absolute left-0 w-full h-full">
                            <div class="w-1/5 h-full flash">
                                <div class="w-full light"></div>
                            </div>
                            <div class="flex h-full transform -translate-y-full">
                                <a href="{{$link_android}}" class="flex-1 h-full" target="_blank"></a>
                                <a href="{{$link_ios}}" class="flex-1 h-full" target="_blank"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wow bounceInUp" data-wow-duration='2s'>
                <img src="{{ asset('img/phone1.png') }}" alt="" class="mw-345">
            </div>
        </div>
    </div>
</div>
<!-- mobile -->
<div class="py-6 md:hidden">

    <img src="{{ asset('img/logo_mobi.png') }}" alt="" class="mx-auto">
    <div class="bg-top-mobi text-white">
        <div class="pt-48">

            <div class="mw-883 px-12">
                <div class="bg-white-blur border border-white rounded-2xl">
                    <div class="flex-row md:flex justify-center items-center p-6 md:px-0 py-6">
                        <a href="/android" class="block pb-3 md:p-3" target="_blank">
                            <img src="http://127.0.0.1:8000/img/ggplay.png" height="80" alt="" class="mx-auto">
                        </a>
                        <a href="https://apps.apple.com/app/gin-plus/id1566394421" class="block md:p-3" target="_blank">
                            <img src="http://127.0.0.1:8000/img/app_store1.png" height="80" alt="" class="mx-auto">
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class=" w-2/3 mx-auto text-title uppercase">SAFE INVESTMENT GIN LTD</div>

        <div class="w-2/3 m-6 xl:p-12 mx-auto">This is the year 2021, thinking about Crypto has changed. Owning
            cryptocurrency is just by working hard every day.</div>
        <div class="flex justify-center py-6 pb-32">
            <div class="bg-white text-700S22 radius-70 text-black shadow-lg p-6 py-3 absolute z-10">
                <a href="{{route('download')}}" target="_blank">JOIN NOW</a>
            </div>
        </div>

    </div>
</div>

<!-- <div class="hidden md:block">
    <iframe class="mx-auto" width="868" height="424" src="https://www.youtube.com/embed/l1zAU_ACzvY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div> -->
<div class="mw-1112">
    <a href="javascript:;" class="js-video-button block overflow-hidden relative p-6 md:mt-16 xl:mt-0"
        data-video-id='9sQol5pTZCQ'>
        <img src="/img/bg_video.png" alt="" class="mx-auto hidden md:block">
        <div class="md:absolute play_video circle">
            <img src="/img/play_video_button.png" alt="" class="mx-auto">
        </div>
        <div class="absolute play_video not_circle">
            <img src="/img/play_video.png" alt="" class="mx-auto">
        </div>
    </a>
</div>

<div class="relative time-line">
    <div class="">
        <div class="text-center pb-3 roadmap-title">ROADMAP</div>
        <div class="mw-1112 px-6 md:p-0">
            <ul class="flex-row md:flex">
                <li class="w-full md:w-1/5 md:pb-16 flex md:block wow bounceInRight">
                    <div
                        class="w-1/3 md:w-full roadmap-timeline text-center pb-12 py-6 pr-6 md:pr-0 xl:p-6 border-r-2 border-black md:border-0">
                        August 2020
                        <div class="relative md:hidden">
                            <div class="absolute dot-time-line-mobi">
                                <div class="h-6 w-6 rounded-full bg-black"></div>
                            </div>
                        </div>
                    </div>
                    <div class="hidden md:flex items-center">
                        <div class="flex-1 border border-black"></div>
                        <div class="h-6 w-6 rounded-full bg-black"></div>
                        <div class="flex-1 border border-black"></div>
                    </div>
                    <div class="flex-1 roadmap-content pb-12 pl-6 md:p-6 md:pt-12">
                        Start project, research strategy, conceptualize, start whitepaper implementation, build
                        development team.
                    </div>
                </li>
                <li class="w-full md:w-1/5 md:pb-16 flex md:block wow bounceInRight" data-wow-delay="0.2s">
                    <div
                        class="w-1/3 md:w-full roadmap-timeline text-center pb-12 py-6 pr-6 md:pr-0 xl:p-6 border-r-2 border-black md:border-0">
                        April 2021
                        <div class="relative md:hidden">
                            <div class="absolute dot-time-line-mobi">
                                <div class="h-6 w-6 rounded-full bg-black"></div>
                            </div>
                        </div>
                    </div>
                    <div class="hidden md:flex items-center">
                        <div class="flex-1 border border-black"></div>
                        <div class="h-6 w-6 rounded-full bg-black"></div>
                        <div class="flex-1 border border-black"></div>
                    </div>
                    <div class="flex-1 roadmap-content pb-12 pl-6 md:p-6 md:pt-12">
                        The GIN is starting to roll out phase 1 to all users. The first blocks are distributed for free.
                        GIN's goal during this period is to reach 500,000 app downloads and 20 million tokens to be
                        distributed for free.
                    </div>
                </li>
                <li class="w-full md:w-1/5 md:pb-16 flex md:block wow bounceInRight" data-wow-delay="0.4s">
                    <div
                        class="w-1/3 md:w-full roadmap-timeline text-center pb-12 py-6 pr-6 md:pr-0 xl:p-6 border-r-2 border-black md:border-0">
                        October 2021
                        <div class="relative md:hidden">
                            <div class="absolute dot-time-line-mobi">
                                <div class="h-6 w-6 rounded-full bg-black"></div>
                            </div>
                        </div>
                    </div>
                    <div class="hidden md:flex items-center">
                        <div class="flex-1 border border-black"></div>
                        <div class="h-6 w-6 rounded-full bg-black"></div>
                        <div class="flex-1 border border-black"></div>
                    </div>
                    <div class="flex-1 roadmap-content pb-12 pl-6 md:pt-12 md:p-6">
                        App downloads became popular and used by millions of users. GIN tokens are distributed free of
                        charge. The GIN Wallet application completes the payment and animate. Listing GIN on exchanges.
                        Target price 5 USD.
                    </div>
                </li>
                <li class="w-full md:w-1/5 md:pb-16 flex md:block wow bounceInRight" data-wow-delay="0.6s">
                    <div
                        class="w-1/3 md:w-full roadmap-timeline text-center pb-12 py-6 pr-6 md:pr-0 xl:p-6 border-r-2 border-black md:border-0">
                        December 2021
                        <div class="relative md:hidden">
                            <div class="absolute dot-time-line-mobi">
                                <div class="h-6 w-6 rounded-full bg-black"></div>
                            </div>
                        </div>
                    </div>
                    <div class="hidden md:flex items-center">
                        <div class="flex-1 border border-black"></div>
                        <div class="h-6 w-6 rounded-full bg-black"></div>
                        <div class="flex-1 border border-black"></div>
                    </div>
                    <div class="flex-1 roadmap-content pb-6 pl-6 md:p-6 md:pt-12">
                        GIN peer-to-peer exchange launched into the community. Mainnet GIN Blockchain implementation.
                        Deploy GIN miner on CPU and mobile platforms. Transaction fees are fully redistributed to GIN
                        holders and 50% used to assist victims affected by the Covid epidemic - 19
                    </div>
                </li>
                <li class="w-full md:w-1/5 md:pb-16 flex md:block wow bounceInRight" data-wow-delay="0.8s">
                    <div
                        class="w-1/3 md:w-full roadmap-timeline text-center pb-12 py-6 pr-6 md:pr-0 xl:p-6 border-r-2 border-black md:border-0">
                        2022
                        <div class="relative md:hidden">
                            <div class="absolute dot-time-line-mobi">
                                <div class="h-6 w-6 rounded-full bg-black"></div>
                            </div>
                        </div>
                    </div>
                    <div class="hidden md:flex items-center">
                        <div class="flex-1 border border-black"></div>
                        <div class="h-6 w-6 rounded-full bg-black"></div>
                        <div class="flex-1 border border-black"></div>
                    </div>
                    <div class="flex-1 roadmap-content pb-6 pl-6 md:p-6 md:pt-12">
                        A year of prosperous development of the GIN community with a series of extended GIN ecosystems
                        born. Intellectual Property Platform - Non-fungible tokens (NFTs) was deployed to protect
                        content developers and develop Token on the GIN platform. GIN Blockchain era, GIN ecosystem.
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="md:hidden time-line-border pt-6"></div>
</div>

<div class="team">
    <div class="team-title text-center pb-10 uppercase wow bounceInLeft">Co-team</div>
    <div class="mw-1112 wow bounceInUp" data-wow-duration="2s">
        <ul class="flex flex-wrap items-center">
            <li class="w-1/2 md:w-1/4 p-3">
                <div class="rounded-2xl shadow-lg hover:text-white cursor-pointer member">
                    <div class="py-16 px-2">
                        <img src="{{ asset('img/team1.jfif') }}" alt="" width="150" height="150"
                            class="mx-auto border-4 border-white rounded-full">
                        <div class="team-name text-center pt-6 truncate" title="Chan Liam">Chan Liam</div>
                        <div class="team-des text-center pt-2 truncate" title="Market director">Market director</div>
                    </div>
                </div>
            </li>
            <li class="w-1/2 md:w-1/4 p-3">
                <div class="rounded-2xl shadow-lg hover:text-white cursor-pointer member">
                    <div class="py-16 px-2">
                        <img src="{{ asset('img/team2.jfif') }}" alt="" width="150" height="150"
                            class="mx-auto border-4 border-white rounded-full">
                        <div class="team-name text-center pt-6 truncate" title="Jain Mask">Jain Mask </div>
                        <div class="team-des text-center pt-2 truncate" title="Leader">Leader</div>
                    </div>
                </div>
            </li>
            <li class="w-1/2 md:w-1/4 p-3">
                <div class="rounded-2xl shadow-lg hover:text-white cursor-pointer member">
                    <div class="py-16 px-2">
                        <img src="{{ asset('img/team3.jfif') }}" alt="" width="150" height="150"
                            class="mx-auto border-4 border-white rounded-full">
                        <div class="team-name text-center pt-6 truncate" title="Levi Frida">Levi Frida </div>
                        <div class="team-des text-center pt-2 truncate" title="Technical operator">Technical operator
                        </div>
                    </div>
                </div>
            </li>
            <li class="w-1/2 md:w-1/4 p-3">
                <div class="rounded-2xl shadow-lg hover:text-white cursor-pointer member">
                    <div class="py-16 px-2">
                        <img src="{{ asset('img/team4.jfif') }}" alt="" width="150" height="150"
                            class="mx-auto border-4 border-white rounded-full">
                        <div class="team-name text-center pt-6 truncate" title="Walton Odile">Walton Odile</div>
                        <div class="team-des text-center pt-2 truncate" title="Blockchain specialist">Blockchain
                            specialist</div>
                    </div>
                </div>
            </li>
        </ul>
        <div class="text-400S24 text-center p-12 text-blue-500">
            <a href="/team" class="hover:text-blue-300">See More</a>
        </div>
    </div>
</div>
<div class="">
    <div class="team-title text-center pb-10 wow bounceInLeft latest">LASTEST NEWS</div>
    <div class="post">
        @foreach($news as $new)
        <a href="{{route('new.detail',['slug'=> $new->slug])}}" class="flex flex-wrap rounded hover:bg-gray-200">
            <div class="w-full md:w-1/3 p-10 py-3 md:py-10">
                <img src="{{ $new->logo }}" alt="" class="w-full rounded overflow-hidden shadow">
            </div>
            <div class="flex-1 p-10 pt-3 md:pt-10">
                <div class="flex flex-col justify-between h-full">
                    <div class="capitalize font-bold line-camp-3">{{ $new->title }} Lorem ipsum, dolor sit amet
                        consectetur adipisicing elit. Architecto dolorem in saepe sequi autem vero omnis totam? Vel
                        veniam consequatur eum modi nulla explicabo repudiandae enim ea recusandae, sed sunt!</div>
                    <small class="text-gray-500">{{ $new->created_at->format('d/m/Y') }}</small>
                </div>
            </div>
        </a>
        @endforeach
        <div class="text-400S24 text-center p-12 text-blue-500">
            <a href="{{route('news')}}" class="hover:text-blue-300">See More</a>
        </div>
    </div>
</div>

@endsection


@push('js')
<script src="{{ asset('js/particles.min.js') }}"></script>

<script>
    particlesJS("particles-js", {
        "particles": {
            "number": {

                "value": 88,
                "density": {
                    "enable": true,
                    "value_area": 700
                }
            }

            ,
            "color": {
                "value": ["#aa73ff", "#f8c210", "#83d238", "#33b1f8"]
            }

            ,
            "shape": {

                "type": "circle",
                "stroke": {
                    "width": 0,
                    "color": "#000000"
                }

                ,
                "polygon": {
                    "nb_sides": 15
                }
            }

            ,
            "opacity": {

                "value": 1,
                "random": false,
                "anim": {
                    "enable": false,
                    "speed": 1.5,
                    "opacity_min": 0.15,
                    "sync": false
                }
            }

            ,
            "size": {

                "value": 3,
                "random": false,
                "anim": {
                    "enable": true,
                    "speed": 2,
                    "size_min": 0.15,
                    "sync": false
                }
            }

            ,
            "line_linked": {
                "enable": true,
                "distance": 110,
                "color": "#33b1f8",
                "opacity": 0.25,
                "width": 1
            }

            ,
            "move": {

                "enable": true,
                "speed": 2,
                "direction": "none",
                "random": false,
                "straight": false,
                "out_mode": "out",
                "bounce": false,
                "attract": {
                    "enable": false,
                    "rotateX": 600,
                    "rotateY": 1200
                }
            }
        }

        ,
        "interactivity": {

            "detect_on": "canvas",
            "events": {
                "onhover": {
                    "enable": false,
                    "mode": "repulse"
                }

                ,
                "onclick": {
                    "enable": false,
                    "mode": "push"
                }

                ,
                "resize": true
            }

            ,
            "modes": {
                "grab": {

                    "distance": 400,
                    "line_linked": {
                        "opacity": 1
                    }
                }

                ,
                "bubble": {
                    "distance": 400,
                    "size": 40,
                    "duration": 2,
                    "opacity": 8,
                    "speed": 3
                }

                ,
                "repulse": {
                    "distance": 200,
                    "duration": 0.4
                }

                ,
                "push": {
                    "particles_nb": 4
                }

                ,
                "remove": {
                    "particles_nb": 2
                }
            }
        }

        ,
        "retina_detect": true
    }

    );

</script>
@endpush