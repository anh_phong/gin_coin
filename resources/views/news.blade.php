@extends('index')

@section('content')
<div class="bg2 mw-1112">
    <div class="layout pb-10">
        <div class="text-gradien uppercase text-center wow bounceInLeft">List News</div>
        <div class="contact-content w-3/4 xl:w-1/2 mx-auto text-content wow bounceInUp" data-wow-duration="2s">
            <ul>
                @foreach($news as $k => $new)
                    <li>
                        <a
                            href="{{ route('new.detail',['slug'=>$new->slug]) }}">
                            <div class="font-bold text-xl truncate py-3">{{ $k+1 }}.{{ " ".$new->title }}</div>
                        </a>
                    </li>
                @endforeach
            </ul>
            @if ($panigate['total_page'] > 1)
                <ul class="flex justify-center items-center pt-6">
                    @if ($panigate['page'] > 1)
                        <li>
                            <a class="block w-8 h-8 flex justify-center items-center mx-1 rounded-full hover:bg-gray-300 " href="{{route('news',['page'=>$panigate['page']-1])}}"><</a>
                        </li>
                    @endif
                    @foreach ($panigate['range_page'] as $k => $page)
                        <li>
                            <a class="block w-8 h-8 flex justify-center items-center mx-1 rounded-full hover:bg-gray-300 {{ $panigate['page'] == $page ? 'bg-gray-300' : '' }}" href="{{route('news',['page'=>$page])}}">{{$page}}</a>
                        </li>
                    @endforeach
                    @if ($panigate['page'] < $panigate['total_page'])
                        <li>
                            <a class="block w-8 h-8 flex justify-center items-center mx-1 rounded-full hover:bg-gray-300" href="{{route('news',['page'=>$panigate['page']+1])}}">></a>
                        </li>
                    @endif
                </ul>
            @endif
        </div>
    </div>

</div>
@endsection
