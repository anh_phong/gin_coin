@extends('index')

@section('content')
<div class="bg2 mw-1112">
    <div class="layout pb-10">
        <div class="text-gradien uppercase text-center wow bounceInLeft">News</div>
        <div class="contact-content w-3/4 xl:w-1/2 mx-auto text-content wow bounceInUp" data-wow-duration="2s">
            <div class="min-h-screen">
                {!! $new->content !!}
            </div>
        </div>
    </div>

</div>
@endsection
